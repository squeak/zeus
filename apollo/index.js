var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/array");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

class Apollo {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INITIALIZE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: apollo - turning arrows into tablify configurations
    ARGUMENTS: ({
      !descartes,
      !pouch: <pouchdb>,
      !dionisos,
      // ?presetArrows: <{ [presetName <string>]: <arrow> }>, // TODO: this could be implemented as an alternative place to look for arrows configurations
      ?defaultGrape: <grape>,
      ?openMultipleArrowsDebugger <see zeus/index.js>,
    })
    RETURN: <void>
  */
  constructor (apolloConfig) {
    var apollo = this;
    _.each(apolloConfig, function (val, key) { apollo[key] = val; });
  }

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET ARROW
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION:
      apollo - turning arrows into tablify configurations
      get arrow, get it's grapes, autofill them...
    ARGUMENTS: ({
      !databaseName: <string> « name of the database to fetch config for »,
      !localTablifyOptions: <tablify·options> « see zeus.getTablifyOptions·localTablifyOptions »,
    })
    RETURN: <void>
  */
  getArrow (options) {
    var apollo = this;

    return apollo.pouch.find_withAttachments({
      selector: { name: options.databaseName, type: { $in: ["database", "database-home"], }, },
      // fields: ['_id', 'name'],
      // sort: ['name']
      manyFound: apollo.openMultipleArrowsDebugger,
      noneFoundThrow: { error: "arrow-not-found", },
    }).then(function (doc) {
      // make full arrow
      var entry = $$.defaults(options.localTablifyOptions, doc);
      // custom behaviour for toolbarButtons, merge the ones from both sources
      if (options.localTablifyOptions.toolbarButtons && doc.toolbarButtons) entry.toolbarButtons = $$.array.merge(options.localTablifyOptions.toolbarButtons, doc.toolbarButtons);
      // process arrow and return it
      return apollo.convertArrowToTablifyConfig(entry);
    });

  }

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  PROCESS ARROW
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: add/convert all necessary parts for an arrow to be given to tablify
    ARGUMENTS: (
      !arrow <arrow> « arrow to convert »,
    )
    RETURN: <Promise:<tablify·options>>
    TYPES: arrow type is described in apollo/arrow.squyntax
  */
  convertArrowToTablifyConfig (arrow) {
    var apollo = this;

    // if paths database, add folder type
    if (arrow.pathsDatabase) {

      // make sure there is folder type if paths database
      if (_.indexOf(arrow.availableTypes, "folder") == -1) {
        if (!arrow.availableTypes) arrow.availableTypes = [];
        arrow.availableTypes.push("folder");
      };

      // if entryTypes used, fetch folder if paths database
      if (arrow.entryTypes && _.indexOf(arrow.entryTypes, "folder") == -1) arrow.entryTypes.push("folder");

    };

    // replace any value that starts with "zeus+" with the corresponding method (do this before adding grapes, because grapes methods are autofilled by dionisos.autofillGrape)
    apollo.descartes.autofillMethods(arrow);

    // fill arrow entryTypes
    return apollo.dionisos.getGrapes(arrow.entryTypes || arrow.availableTypes).then(function (grapes) {

      arrow.entryTypes = grapes;

      // make sure arrow has defaultType
      if (!arrow.defaultType) return new Promise(function (resolve, reject) {
        apollo.dionisos.autofillGrape(_.clone(apollo.defaultGrape), function (filledDefaultGrape) {
          arrow.defaultType = filledDefaultGrape;
          resolve(arrow);
        });
      });
      else return arrow;

    }).catch(function (err) {
      $$.log.detailedError(
        "zeus/apollo.convertArrowToTablifyConfig",
        "Failed to convert arrow to tablify config.",
        err
      )
    });

  }

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = Apollo;
