# zeus

Generate full featured tablify configurations from json config files stored in a database.

For the full documentation, installation instructions... check [zeus documentation page](https://squeak.eauchat.org/libs/zeus/).
