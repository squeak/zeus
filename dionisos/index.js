var _ = require("underscore");
var $$ = require("squeak");
var async = require("async");
var utilities = require("./utilities");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

class Dionisos {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INITIALIZE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: dionisos - turning grapes into entry types for tablify
    ARGUMENTS: ({
      !descartes,
      !pouch <pouchdb>,
      !hermes,
      ?presetsGrapes,
      ?openMultipleGrapesDebugger,
    })
    RETURN: <void>
  */
  constructor (dionisosConfig) {
    var dionisos = this;
    _.each(dionisosConfig, function (val, key) { dionisos[key] = val; });
  }

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET GRAPES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get multiple grapes
    ARGUMENTS: ( ?typeNamesList <string[]>, )
    RETURN: <Promise:<grape[]>>
  */
  getGrapes (typeNamesList) {
    var dionisos = this;

    return new Promise(function (resolve, reject) {

      dionisos.pouch.filter_withAttachments({
        selector: { type: "type", name: { $in: typeNamesList, }, },
      }).then(function (docs) {

        // make list of asked grapes
        var grapes = utilities.findGrapes(typeNamesList, docs, dionisos.presetsGrapes);

        // check if some type names correspond to more than one configuration
        var allTypes = _.groupBy(docs, "name");
        _.each(allTypes, function (types, typeName) {
          if (types.length > 1) dionisos.openMultipleGrapesDebugger(typeName, types);
        });

        // make sure grapes have also subgrapes filled
        async.map(grapes, function (grape, done) {
          dionisos.autofillGrape(grape, function (finalGrape) {
            done(null, finalGrape);
          });
        }, function (err, grapesMapped) {
          if (err) reject(err);
          resolve(grapesMapped);
        });

      }).catch(function (err) {
        $$.log.detailedError(
          "zeus/dionisos.getGrapes",
          "Failed to fetch grapes.",
          err
        );
        return err;
      });

    });

  }

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET GRAPE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get a single grape
    ARGUMENTS: ( ?typeName <string>, )
    RETURN: <Promise:<grape>>
  */
  getGrape (typeName) {
    var dionisos = this;

    return new Promise(function (resolve, reject) {

      dionisos.pouch.find_withAttachments({
        selector: { name: typeName, type: "type", },
        manyFound: dionisos.openMultipleGrapesDebugger,
      }).then(function (doc) {

        // grape found
        var grape = utilities.findGrape(typeName, [doc], dionisos.presetsGrapes);

        // make sure grape has also subgrapes filled
        dionisos.autofillGrape(grape, resolve);

      }).catch(function (err) {
        $$.log.detailedError(
          "zeus/dionisos.getGrapes",
          "Failed to fetch grapes.",
          err
        );
        reject(err);
      });

    });

  }

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  AUTOFILL GRAPE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: fill all subgrapes structure, methods
    ARGUMENTS: (
      !edifyConfig <grape>,
      callback <function(<grape>)>
    )
    RETURN: <void>
  */
  autofillGrape (grape, callback) {
    var dionisos = this;
    var grapeWithoutAutofilling = $$.clone(grape, 99);
    var resultGrape = _.clone(grape);
    dionisos.autofillEdifyStructure(resultGrape.edit, function () {

      // replace any value that starts with "zeus+" with the corresponding method
      dionisos.descartes.autofillMethods(resultGrape);

      // store a version of the grape without autofilled values
      resultGrape.grapeWithoutAutofilling = grapeWithoutAutofilling;

      // callback with resulting grape
      callback(resultGrape);

    });
  }

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  AUTOFILL EDIFY STRUCTURE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/


  /**
    DESCRIPTION: fill all edifyConfig subgrapes structure
    ARGUMENTS: (
      !edifyConfig <grape.edit>,
      !callback <function(ø):void> « nothing is passed in this callback because the edifyConfig object is modified directly »,
    )
    RETURN: <void>
  */
  autofillEdifyStructure (edifyConfig, callback) {
    var dionisos = this;

    // build structure
    if ($$.getValue(edifyConfig, "structure.length")) async.eachOf(edifyConfig.structure, function (sandalOrSandalName, index, eachCallback) {

      dionisos.hermes.makeSandal(sandalOrSandalName, dionisos, function (inputifyOptions) {
        edifyConfig.structure[index] = inputifyOptions;
        eachCallback();
      });

    }, function (err) {
      if (err) console.error(err);
      // callback grape
      callback();
    })

    // no structure to build
    else callback();

  }

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = Dionisos;
