var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function getPresetSandalArguments (fullName) {
  // get arguments
  var args = ($$.match(fullName, /[^\(]*\)$/) || "").replace(/\)$/, "").split(/\s*,\s*/);
  // if argument is json preocessable, parse it, if not just keep it as is
  return _.map(args, $$.json.parseIfJson);
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var utilities = {

  getPresetSandal: function (sandalOrSandalName, presetSandals) {
    var presetSandal = $$.getValue(presetSandals, utilities.getPresetSandalName(sandalOrSandalName));
    if (_.isFunction(presetSandal)) var sandal = presetSandal.apply(null, getPresetSandalArguments(sandalOrSandalName))
    else var sandal = presetSandal;
    if (!sandal) $$.log.detailedError(
      "zeus/hermes/utilities:getPresetSandal",
      "Could not figure out the preset sandal you asked: ",
      sandalOrSandalName,
      "All available preset sandals: ",
      presetSandals
    )
    else return sandal;
  },

  getPresetSandalName: function (fullName) {
    return $$.match(fullName, /^[^\(]*/);
  },

  typeIs: function (sandal, types) {
    return _.indexOf(types, sandal.type || sandal.inputifyType) != -1;
  },

  // addGrapeChild: function (dionisos, sandalOrModel, callback) {
  //   dionisos.getGrape(sandalOrModel.entryType).then(function (subgrape) {
  //     sandalOrModel.object = subgrape.edit;
  //     callback();
  //   });
  // },

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = utilities;
