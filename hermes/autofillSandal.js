var _ = require("underscore");
var $$ = require("squeak");
var utilities = require("./utilities");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: autofill various inputify options parts from a sandal: entry and structures subgrapes, entry.customizedOptions...
  ARGUMENTS: (
    !sandal « the sandal to modify to turn it into proper inputifyOptions »,
    !dionisos,
    !callback <function(ø):<void>> « nothing passed to callback, sandal is modified directly »,
  )
  RETURN: <void>
*/
function autofillSandal (sandal, dionisos, callback) {

  // get object grape and callback edify part of it
  function getObjectGrape (sandalToGetObjectGrapeFor, next) {
    dionisos.getGrape(sandalToGetObjectGrapeFor.entryType).then(function (subgrape) {
      next(subgrape.edit);
    });
  };

  //
  //                              PARTS METHODS

  // has "entryType"
  function autofill_entryType (next) {
    if (sandal.entryType) {
      // if entry, just pass a function to construct grape on request (when the entry is opened)
      if (utilities.typeIs(sandal, ["entry"])) {
        sandal.object = _.partial(getObjectGrape, sandal);
        next();
      }
      // else construct it all now, because non "entry" inputs cannot support asynchronous getting of options
      else getObjectGrape(sandal, function (inputifyObjectOptions) {
        sandal.object = $$.defaults(inputifyObjectOptions, sandal.object);
        next();
      });
    }
    else next();
  };

  // has "object.model.entryType"
  function autofill_objectModel (next) {
    if ($$.getValue(sandal, "object.model.entryType")) {
      // if entry, just pass a function to construct grape on request (when the entry is opened)
      if (utilities.typeIs(sandal.object.model, ["entry"])) {
        sandal.object.model.object = _.partial(getObjectGrape, sandal.object.model);
        next();
      }
      // else construct it all now, because non "entry" inputs cannot support asynchronous getting of options
      else getObjectGrape(sandal.object.model, function (inputifyObjectOptions) {
        sandal.object.model.object = $$.defaults(inputifyObjectOptions, sandal.object.model.object);
        next();
      });
    }
    else next();
  };

  // has "object.structure"
  function autofill_objectStructure (next) {
    if ($$.getValue(sandal, "object.structure")) dionisos.autofillEdifyStructure(sandal.object, next);
    else next();
  };

  // has "entry.customizedOptions"
  function autofill_customizedEntryOptions (next) {
    if (sandal.entry && sandal.entry.customizedOptions) autofillSandal(sandal.entry.customizedOptions, dionisos, next);
    else next();
  };

  //
  //                              AUTOFILL PARTS AND CALLBACK

  autofill_entryType(function () {
    autofill_objectModel(function () {
      autofill_objectStructure(function () {
        autofill_customizedEntryOptions(function () {
          callback();
        });
      });
    });
  });

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = autofillSandal;
