var _ = require("underscore");
var $$ = require("squeak");
var utilities = require("./utilities");
var sandalDefaults = require("./defaults");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: get a sandal from a <string> or { inputPreset: <string>, } or <sandal>
  ARGUMENTS: (
    !sandalOrSandalName <sandalOrSandalName>,
    !presetSandals <{ [sandalName <string>]: <sandal> }>
  )
  RETURN: <sandal>
*/
function getSandal (sandalOrSandalName, presetSandals) {

  // only a string has been passed, get preset sandal from it
  if (_.isString(sandalOrSandalName)) {
    var sandal = utilities.getPresetSandal(sandalOrSandalName, presetSandals);
    // missing sandal preset: make default sandal
    if (!sandal) sandal = { key: utilities.getPresetSandalName(sandalOrSandalName), };
  }
  // an object requiring a preset has been passed
  else if (_.isObject(sandalOrSandalName) && sandalOrSandalName.inputPreset) {
    var sandal = $$.defaults(utilities.getPresetSandal(sandalOrSandalName.inputPreset, presetSandals), sandalOrSandalName);
    // missing sandal preset: add passed name
    if (!sandal.key && !sandal.name && !sandal.presetWithoutKey) sandal.key = utilities.getPresetSandalName(sandalOrSandalName.inputPreset);
  }
  // sandal has been passed
  else sandal = sandalOrSandalName;

  // make a new copy of the given sandal
  var sandal = $$.clone(sandal, 5);

  // recurse if necessary
  if (sandal.object) {
    if (sandal.object.model) sandal.object.model = getSandal(sandal.object.model, presetSandals);
    if (sandal.object.structure) sandal.object.structure = _.map(sandal.object.structure, function (sand) { return getSandal(sand, presetSandals); });
  };

  // return sandal;
  return $$.defaults(sandalDefaults(), sandal);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = getSandal;
