var _ = require("underscore");
var $$ = require("squeak");

var makePouch = require("./pouch");

var Apollo = require("../apollo");
var Dionisos = require("../dionisos");
var Hermes = require("../hermes");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION:
    initialize zeus (instantiate it)
    zeus exposes the API allowing to get proper tablify config
      - from their name: from dato database,
      - passing a partial config and autofilling it
  ARGUMENTS: ({
    !descartes: <descartes>,
    ?presets: <{
      ?arrows: <{ [presetName <string>]: <arrow>, }> «
        list of arrow presets, additional list to fetch configs // TODO: not yet implemented
      »,
      ?grapes: <grape[]> «
        list of grapes that will always be defined, whatever the db contain, if types with same name as those ones are present in the db, db has precedence
        this list should contain a grape named "default" that will be used as default type to pass to tablify
      »,
      ?sandals: <{ [presetName <string>]: <sandal>, }> «
        list of sandal presets, that can be setup to create more easily inputify configurations shorthands
      »,
    }>,
    ?openMultipleArrowsDebugger: <function(configsFound<arrow[]>) «
      use this method to setup a custom action that will be ran when multiple arrow configurations matching your query have been found in the database,
      the first entry found will still be used this time, but this gives you the opportunity to examine and modify or remove the inapropriate entries
    »,
    ?openMultipleGrapesDebugger: <function(configsFound<grape[]>) «
      use this method to setup a custom action that will be ran when multiple grape configurations matching your query have been found in the database,
      the first entry found will still be used this time, but this gives you the opportunity to examine and modify or remove the inapropriate entries
    »,
  })
  RETURN: <zeus> « see below for list of methods available in zeus »
*/
module.exports = function (config) {
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  // make sure presets object is at least defined
  if (!config.presets) config.presets = {};

  // create pouch database
  var pouch = makePouch(config.descartes);

  // INSTANTIATE APOLLO, DIONISOS AND HERMES
  var hermes = new Hermes({
    presetSandals: config.presets.sandals,
  });
  var dionisos = new Dionisos({
    descartes: config.descartes,
    pouch: pouch,
    hermes: hermes,
    presetsGrapes: config.presets.grapes,
    openMultipleGrapesDebugger: config.openMultipleGrapesDebugger,
  });
  var apollo  = new Apollo({
    descartes: config.descartes,
    pouch: pouch,
    dionisos: dionisos,
    presetArrows: config.presets.arrows,
    defaultGrape: _.findWhere(config.presets.grapes, { name: "default", }),
    openMultipleArrowsDebugger: config.openMultipleArrowsDebugger,
  });

  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  return {
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    /**
      DESCRIPTION: get full tablify options for a database
      ARGUMENTS: (
        !databaseName <string> « name of the database to fetch config for »,
        !localTablifyOptions <tablify·options> «
          - any tablify options that you want hardcoded (but these options will not have priority over the one fetched)
          - after those, you will at least need to set $container and $toolbarContainer keys if you want fully functional tablify options
        »,
      )
      RETURN: <Promise:<tablify·config>>
    */
    getTablifyOptions: function (databaseName, localTablifyOptions) {
      return apollo.getArrow({
        databaseName: databaseName,
        localTablifyOptions: localTablifyOptions,
      });
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    /**
      DESCRIPTION: autofill entryTypes, and defaultType for the passed tablify options
      ARGUMENTS: ( !tablifyOptions <tablify·config> « the options you want to autofill some values in », )
      RETURN: <Promise:<tablify·config>>
    */
    autofillTablifyOptions: function (tablifyOptions) {
      return apollo.convertArrowToTablifyConfig(tablifyOptions, dionisos);
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  };

  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
};
