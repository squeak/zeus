var _ = require("underscore");
var $$ = require("squeak");
var async = require("async");

var PouchDB = require("pouchdb");
var PouchFind = require("pouchdb-find");
PouchDB.plugin(PouchFind);
// PouchDB.plugin(require("pouchdb-debug")); // TO USE THIS, NEEDS TO INSTALL pouchdb-debug MODULE FROM NPM
// PouchDB.debug.enable('*');

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: extend pouch with a few additional nice methods
  ARGUMENTS: ( pouch )
  RETURN: <void>
*/
function extendPouch (pouch) {

  /**
    DESCRIPTION: find the first entry matching the given selector, with it's attachments
    ARGUMENTS: ({
      !selector: <object>,
      ... any other option that pouch.find accepts
      ?manyFound: <function(entriesFound <couchEntry[]>):<void>> «
        will be executed if multiple entry found
        this is optional
      »,
      ?noneFoundThrow: <object|any> «
        error to throw if no entry was found,
        if this is not defined, will not throw any error
      »,
    })
    RETURN: <Promise>
  */
  pouch.find_withAttachments = function (options) {
    return pouch.find(options).then(function (result) {

      // some entry was found
      if (result.docs.length) {
        if (result.docs.length > 1 && options.manyFound) options.manyFound(result.docs);
        var doc = result.docs[0];
        if (_.keys(doc._attachments).length) return pouch.get(doc._id, { attachments: true, })
        else return doc;
      }

      // if not found, throw error if asked to
      else if (options.noneFoundThrow) throw options.noneFoundThrow;

    });
  };

  /**
    DESCRIPTION: find the first entry matching the given selector, with it's attachments
    ARGUMENTS: ({
      !selector: <object>,
      ... any other option that pouch.find accepts
    })
    RETURN: <Promise>
  */
  pouch.filter_withAttachments = function (options) {
    return pouch.find(options).then(function (result) {

      return new Promise(function(resolve, reject) {

        // bulkGet doesn't seem to work when you want attachments, don't understand why, and it's really weird the way it responds, so do like this for now
        async.map(result.docs, function (doc, callback) {
          if (_.keys(doc._attachments).length) pouch.get(doc._id, { attachments: true, }).then(function (couchEntryWithAttachments) {
            callback(null, couchEntryWithAttachments)
          }).catch(reject);
          else callback(null, doc)
        }, function (err, list) {
          if (err) reject(err);
          resolve(list || []);
        });

      });

    });
  };

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  REFRESH LIST OF DESCARTES METHODS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function refreshListOfDescartesMethods (pouch, descartes) {

  pouch.find({
    selector: { type: "_method_", },
  }).then(function (response) {

    descartes.config.additionalMethods = [];
    _.each(response.docs, function (entry) {
      descartes.config.additionalMethods.push(entry);
    });

  }).catch(function (err) {
    $$.log.error("[zeus] Failed to refresh list of methods in descartes:", err);
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SUBSCRIBE TO POUCH CHANGES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function customizeForZeus (pouch, descartes) {
  pouch.isZeusEnabled = true;

  // extend pouch
  extendPouch(pouch);

  // initially refresh list of descartes methods
  refreshListOfDescartesMethods(pouch, descartes);

  // watch changes to dato pouch to refresh list of additional methods in descartes
  var throttledRefreshListOfDescartesMethods = _.throttle(refreshListOfDescartesMethods, 400);
  pouch.changes({
    live: true,
    since: "now",
  }).on("change", function () {
    throttledRefreshListOfDescartesMethods(pouch, descartes);
  });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (descartes) {

  // figure out pouch name for this user
  var pouchName = descartes.config.username +"--dato";

  // if dato db for this user has already been created, return it (making sure it has zeus customizations)
  if (descartes.config.pouchsCache[pouchName]) {
    if (!descartes.config.pouchsCache[pouchName].isZeusEnabled) customizeForZeus(descartes.config.pouchsCache[pouchName], descartes);
    return descartes.config.pouchsCache[pouchName];
  }

  // else create it and customize if for zeus needs
  else {
    var pouch = descartes.config.pouchsCache[pouchName] = new PouchDB(pouchName);
    pouch.setMaxListeners(20); // avoid having some "error" logged that there is a memory leak, but maybe this should be verified some day
    customizeForZeus(pouch, descartes);
    return pouch;
  };

};
