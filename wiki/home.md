
Ok, so let's say you'd like to display, and edit a database and it's entries in a nice way. But you don't want to have to code an app for each new database you'll make.  
So you could make an app, that allow you to create graphically, configurations for databases' display and edition.  
You could store those configurations in a database.  
Then you could just convert those configurations into something that some libraries can interpret to make a nice UI for displaying and editing the database.  

This is what Zeus does!
Zeus is here to fetch a configuration, interpret it, and make sure it's suitable to be injected into [tablify](/tablify/). Zeus is a core building block of the [dato](/dato/) app.
