# Arrows, grapes and sandals
🛈 This section uses squyntax notation, information/documentation about it is available [here](/squyntax/). 🛈

Arrows, grapes and sandals are the types of configurations that Apollo, Dionisos and Hermes can process, to convert them into the proper configuration pieces to be passed to tablify.


## Arrow

The arrow is the item of Apollo, processed by Apollo, it produces a [tablify](/tablify/) configuration object.

An arrow object is the of the same type as a `tablify·config` object, plus:
It can support writing functions in the following way: `"zeus+meth.od.name(arg, u, ments)"`.
Where:
  - `zeus+` is the starting code, it's always there
  - `meth.od.name` is the deep key of the method to use (that key should exist in descartes)
  - `arg, u, ments` is the list of values that will be passed to the method when executing it.

The `entryTypes` option doesn't have to be set, Apollo will create it from the given list of `availableTypes`. However, if an `entryTypes` array is passed, it should contain strings containing the names of the types of configurations to fetch, and it will bypass the `availableTypes` list (this allow to have more entry types fetched than available types in the UI which can be handy when sometimes you want to set weird custom behaviors in your app).


## Grape

The grape is the item of Dionisos, processed by Dionisos, it produces an object that can be passed to fill the availableTypes option of tablify. This object contains configurations for display in tablify, and also [postify](postify), and [viewify](viewify) configs to view or edit the entry.

A grape object is the of the same type than the `entryType` objects documented in [tablify](/tablify/) API, plus:
It can support writing functions in the following way: `"zeus+meth.od.name(arg, u, ments)"`.
Where:
  - `zeus+` is the starting code, it's always there
  - `meth.od.name` is the deep key of the method to use (that key should exist in descartes)
  - `arg, u, ments` is the list of values that will be passed to the method when executing it

Also a grape should/may contain the following keys:
```javascript
!name: <string>,
?group: <string> « just a group for ordering in the list of types and make it more readable »,
```


## Sandal

The sandal is the item of Hermes, processed by Hermes, it produces an [inputify](/inputify/) configuration object.

A sandal object is the same as an `inputify·options` object.
In addition to this, you can also use the following keys:
```javascript
?inputPreset: <string> «
  key of config.presets.sandals
  if this is set, will get the preset called like this,
  other keys specified in the sandal will be added and are prioritized over the ones from the preset
  creating some preset inputs is a good way to reuse some pieces of codes and not have to write many times the same inputs configurations
»,
?entryType: <string> «
  name of a type entry in types database
  if this is set, will autofill the "object" key with the config of the chosen grape,
  options of the asked entry type and the one passed in this input "object" key (if defined) are merged, with priority given to the ones here
  but if "inputifyType" is "entry" then the "object" options passed in here will be replaced by the ones of the gotten entry type
»,
```
