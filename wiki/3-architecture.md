# Zeus' "employees"

### Apollo
(making [tablify](/tablify/) configuration)

To find a database's tablify configuration, Zeus calls Apollo.  
Apollo is in charge of finding the configuration in the database containing configurations, making sure that it is complete, that it contains the list of types that entries can have in this database, get each type's configuration, interpret custom methods...

### Dionisos
(making [postify](/postify/), [viewify](/viewify/) configurations and more)

To find the view and edition configuration for each type of entry allowed in a database, Apollo calls Dionisos.
Dionisos will find the entry type's configuration and make sure it is complete, unfold presets, interpret custom methods...
A type configuration is made of some general elements, defining the entry title, subtitle, icon, color... a configuration on how to view this entry, and another one defining how to edit (= the list of inputs to display in edition page)...

### Hermes
(making [inputify](/inputify/) configurations)

Hermes is called by Dionisos to make sure that all input's configurations (the inputs in the entry edition page) contain everything they should, unfolding input's preset configs if necessary...

## A bit of mythology

*Why Apollo, Dionisos and Hermes?*

Well let's say that each entry is a poetry:

A poetry being made of words, Hermes, god of travels and language structure the elements of the poetry (each line/input).
And Dionisos, obviously, being the god of poetry, is ruling how a poetry should be (viewed and written).
Then comes Apollo, sun god, who reigns on the whole landscape of poetries, the display of the database.
And to finish, on top of this whole well organized society ;) comes Zeus who orders everything around, asks what he wants to have to the others. This is why if you want something, the quickest way to get it, is to call Zeus directly.

## What does all this shit bring? Why do you need Zeus and don't write and store directly the configurations for tablify, postify, whateverify...

One of the reason for the necessity of Zeus, is simply to make the connection with the database containing configurations very smooth and direct.  

But then also tablify, postify, viewify and inputify libraries sometimes receive options that are functions or other complex element that cannot be represented in the json structures stored in the database. Zeus, here, with the help of [Descartes](/descartes/), provides a set of complex methods that give real power to the end user to handle their database and make complex displays/inputs. (A method name, is coded in the json structure like this: "zeus+meth.od.name(arg, u, ments)", where "met.od.name" must be and existing method in [descartes](/descartes/).)

Zeus is also very handy when you need to reuse many time the same configuration for an input, you just create a preset and then use it, doing so you don't need to write the same configuration over and over.
