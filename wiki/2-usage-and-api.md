# Usage and API
🛈 This page uses squyntax notation, information/documentation about it is available [here](/squyntax/). 🛈

## Usage

To use zeus, first you need to instantiate it passing it a configuration:
```javascript
const zeusMaker = require("zeus");
const zeus = zeusMaker(myZeusConfig);
```

Then use it with something like this:
```javascript
zeus.getTablifyOptions("my-database-name").then((tablifyConfig) => {
  console.log(tablifyConfig);
}).catch(console.log.bind(console));
```

## Instantiation configuration

```javascript
zeusConfig = {
  !username: <string> « should be all lower case and non-empty string »,
  ?presets: <{
    ?arrows: <{ [presetName <string>]: <arrow>, }> «
      list of arrow presets, additional list to fetch configs // TODO: not yet implemented
    »,
    ?grapes: <grape[]> «
      list of grapes that will always be defined, whatever the db contain, if types with same name as those ones are present in the db, db has precedence
      this list should contain a grape named "default" that will be used as default type to pass to tablify
    »,
    ?sandals: <{ [presetName <string>]: <sandal>, }> «
      list of sandal presets, that can be setup to create more easily inputify configurations shorthands
    »,
  }>,
  ?openMultipleArrowsDebugger: <function(configsFound<arrow[]>) «
    use this method to setup a custom action that will be ran when multiple arrow configurations matching your query have been found in the database,
    the first entry found will still be used this time, but this gives you the opportunity to examine and modify or remove the inapropriate entries
  »,
  ?openMultipleGrapesDebugger: <function(configsFound<grape[]>) «
    use this method to setup a custom action that will be ran when multiple grape configurations matching your query have been found in the database,
    the first entry found will still be used this time, but this gives you the opportunity to examine and modify or remove the inapropriate entries
  »,
}
```

## API

### zeus.getTablifyOptions

Get full tablify configuration from a database name.
The configuration will be fetched from the database and interpreted (autofilled...).

```javascript
ARGUMENTS: (
  !databaseName <string> « name of the database to fetch config for »,
  !localTablifyOptions <tablify·options> «
    - any tablify options that you want hardcoded (but these options will not have priority over the one fetched)
    - after those, you will at least need to set $container and $toolbarContainer keys if you want fully functional tablify options
  »,
)
RETURN: <Promise:<tablify·config>>
```

### zeus.autofillTablifyOptions

Autofill the given tablify configuration, making sure it has the appropriate types configurations, methods...

```javascript
ARGUMENTS: ( !tablifyOptions <tablify·config> « the options you want to autofill some values in », )
RETURN: <Promise:<tablify·config>>
```
